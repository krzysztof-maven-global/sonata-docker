#!/usr/bin/env bash

docker-compose build
docker-compose run app composer install
docker-compose run app doctine:schema:update --force
docker-compose up -d

echo "App running on http://localhost:9000/admin"